package com.example.progmob2020.Crud;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.progmob2020.Model.DefaultResult;
import com.example.progmob2020.Network.GetDataService;
import com.example.progmob2020.Network.RetrofitClientInstance;
import com.example.progmob2020.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MahasiswaUpdateActivity extends AppCompatActivity {
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mahasiswa_update);

        final EditText edNimLama = (EditText)findViewById(R.id.edNimLama);
        final EditText edNama = (EditText)findViewById(R.id.edUpNama);
        final EditText edNim = (EditText)findViewById(R.id.edUpNim);
        final EditText edAlamat = (EditText)findViewById(R.id.edUpAlamat);
        final EditText edEmail = (EditText)findViewById(R.id.edUpEmail);
        Button btnUp = (Button) findViewById(R.id.BtnUpdate);
        pd = new ProgressDialog(MahasiswaUpdateActivity.this);


        Intent data = getIntent();
        if(data != null) {

            edNimLama.setText(data.getStringExtra("nim"));
            edNama.setText(data.getStringExtra("nama"));
            edNim.setText(data.getStringExtra("nim"));
            edAlamat.setText(data.getStringExtra("alamat"));
            edEmail.setText(data.getStringExtra("Email"));


        }


        btnUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pd.setTitle("Mohon menunggu");
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> del= service.delete_mhs(
                        edNimLama.getText().toString(),
                        "72180186"

                );

                del.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        Toast.makeText(MahasiswaUpdateActivity.this,"Berhasil disimpan",Toast.LENGTH_LONG);

                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(MahasiswaUpdateActivity.this,"Error",Toast.LENGTH_LONG);
                    }
                });

                Call<DefaultResult> add= service.add_mhs(
                        edNama.getText().toString(),
                        edNim.getText().toString(),
                        edAlamat.getText().toString(),
                        edEmail.getText().toString(),
                        "kosongkan saja",
                        "72180186"
                );
                add.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(MahasiswaUpdateActivity.this,"Berhasil disimpan",Toast.LENGTH_LONG);
                        Intent intent = new Intent(MahasiswaUpdateActivity.this, MainMhsActivity.class);
                        startActivity(intent);
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(MahasiswaUpdateActivity.this,"Error",Toast.LENGTH_LONG);
                    }
                });
            }
        });
    }


}