package com.example.progmob2020;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.progmob2020.Pertemuan2.ListActivity;
import com.example.progmob2020.Pertemuan2.RecyclerActivity;
import com.example.progmob2020.Pertemuan2.cardviewtestactivity;
import com.example.progmob2020.Pertemuan4.DebuggingActivity;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //var
        final TextView txtView = (TextView)findViewById(R.id.mainActivityTextView);
        final Button myBtn = (Button)findViewById(R.id.button1);
        final EditText myEditText = (EditText)findViewById(R.id.editTextText1);
        Button btnHelp = (Button)findViewById(R.id.BtnHelp);
        Button btnTrck = (Button)findViewById(R.id.btnTracker);
        Button btnRecl = (Button)findViewById(R.id.btnRec);
        Button btnLs = (Button)findViewById(R.id.btnListView);
        Button btnCard = (Button)findViewById(R.id.btnCard);
        Button btnP4 = (Button)findViewById(R.id.btnP4);



        //act
        txtView.setText(R.string.testkonstanta);
        myBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Log.d("Coba klikk",myEditText.getText().toString());
                txtView.setText(myEditText.getText().toString());
            }
        });
        btnHelp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,HelpActivity.class);
                Bundle b = new Bundle();
                b.putString("hlp_string",myEditText.getText().toString());
                intent.putExtras(b);
                startActivity(intent);
            }
        });
        btnTrck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,TrackerActivity.class);
                startActivity(intent);
            }
        });
        btnLs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, ListActivity.class);
                startActivity(intent);
            }
        });
        btnRecl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, RecyclerActivity.class);
                startActivity(intent);
            }
        });
        btnCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, cardviewtestactivity.class);
                startActivity(intent);
            }
        });
        btnP4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, DebuggingActivity.class);
                startActivity(intent);
            }
        });
    }
}