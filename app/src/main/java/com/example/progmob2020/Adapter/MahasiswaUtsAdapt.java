package com.example.progmob2020.Adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;

import com.example.progmob2020.Crud.HapusMhsActivity;
import com.example.progmob2020.Crud.MahasiswaUpdateActivity;
import com.example.progmob2020.Model.DefaultResult;
import com.example.progmob2020.Model.Mahasiswa;
import com.example.progmob2020.Network.GetDataService;
import com.example.progmob2020.Network.RetrofitClientInstance;
import com.example.progmob2020.ProjectUts.EditMhsActivity;
import com.example.progmob2020.ProjectUts.MainUtsActivity;
import com.example.progmob2020.ProjectUts.MhsViewActivity;
import com.example.progmob2020.R;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MahasiswaUtsAdapt extends RecyclerView.Adapter<MahasiswaUtsAdapt.ViewHolder> {
    private Context context;
    private List<Mahasiswa> mahasiswaList;
    ProgressDialog pd;

    public MahasiswaUtsAdapt(Context context) {
        this.context = context;
        mahasiswaList = new ArrayList<>();
    }

    public MahasiswaUtsAdapt(List<Mahasiswa> mahasiswaList) {
        this.mahasiswaList = mahasiswaList;
    }

    public List<Mahasiswa> getMahasiswaList() {
        return mahasiswaList;
    }

    public void setMahasiswaList(List<Mahasiswa> mahasiswaList) {
        this.mahasiswaList = mahasiswaList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_card_uts,parent,false);

        return new  ViewHolder(v,parent.getContext());
    }

    @Override
    public void onBindViewHolder(@NonNull MahasiswaUtsAdapt.ViewHolder holder, int position) {
        Mahasiswa m = mahasiswaList.get(position);
        holder.tvNamaM.setText(m.getNama());
        holder.tvEmailM.setText(m.getEmail());
        holder.mhs=m;
        pd = new ProgressDialog(holder.context1);
        holder.cToolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Mahasiswa m = mahasiswaList.get(position);
                PopupMenu popupMenu = new PopupMenu(holder.context1,holder.cToolbar);
                popupMenu.inflate(R.menu.menu_card);
                popupMenu.show();
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                        switch (menuItem.getItemId()){
                            case R.id.CMenuDel:
                                pd.setTitle("Deleting");
                                pd.show();
                                Call<DefaultResult> del= service.delete_mhs(
                                        m.getNim().toString(),
                                        "72180186"

                                );
                                del.enqueue(new Callback<DefaultResult>() {
                                    @Override
                                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                                        pd.dismiss();
                                        Toast.makeText(holder.context1,"success",Toast.LENGTH_LONG).show();
                                        Intent intent = new Intent(holder.context1, MainUtsActivity.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        holder.context1.startActivity(intent);
                                    }

                                    @Override
                                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                                        pd.dismiss();
                                        Toast.makeText(holder.context1,"failed",Toast.LENGTH_LONG).show();
                                    }
                                });
                                break;
                            case R.id.CMenuView:
                                Intent UpInput =new Intent(holder.context1, MhsViewActivity.class);
                                UpInput.putExtra("nim",m.getNim());
                                UpInput.putExtra("nama",m.getNama());
                                UpInput.putExtra("alamat",m.getAlamat());
                                UpInput.putExtra("Email",m.getEmail());
                                holder.context1.startActivity(UpInput);
                            break;
                            case  R.id.CMenuEdit:
                                Intent EdInput =new Intent(holder.context1, EditMhsActivity.class);
                                EdInput.putExtra("nim",m.getNim());
                                EdInput.putExtra("nama",m.getNama());
                                EdInput.putExtra("alamat",m.getAlamat());
                                EdInput.putExtra("Email",m.getEmail());
                                holder.context1.startActivity(EdInput);
                        }
                        return false;
                    }
                });
            }
        });
    }

    @Override
    public int getItemCount() {
        return mahasiswaList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView tvNamaM,tvEmailM;
        private ImageView cToolbar;
        Context context1;
        Mahasiswa mhs;
        public ViewHolder(@NonNull View itemView,Context context) {
            super(itemView);
            tvNamaM = itemView.findViewById(R.id.tvNamaM);
            tvEmailM = itemView.findViewById(R.id.tvEmailM);
            cToolbar = itemView.findViewById(R.id.OptionCard);
            context1=context;


        }
    }
}
