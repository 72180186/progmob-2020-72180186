package com.example.progmob2020.Adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.progmob2020.Model.DefaultResult;
import com.example.progmob2020.Model.Dosen;
import com.example.progmob2020.Model.Mahasiswa;
import com.example.progmob2020.Network.GetDataService;
import com.example.progmob2020.Network.RetrofitClientInstance;
import com.example.progmob2020.ProjectUts.DosUtsActivity;
import com.example.progmob2020.ProjectUts.DosViewActivity;
import com.example.progmob2020.ProjectUts.EditDosActivity;
import com.example.progmob2020.ProjectUts.EditMhsActivity;
import com.example.progmob2020.ProjectUts.LoginUtsActivity;
import com.example.progmob2020.ProjectUts.MainUtsActivity;
import com.example.progmob2020.ProjectUts.MhsUtsActivity;
import com.example.progmob2020.ProjectUts.MhsViewActivity;
import com.example.progmob2020.R;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DosUtsAdapt extends RecyclerView.Adapter<DosUtsAdapt.ViewHolder> {
    private Context context;
    private List<Dosen> dosenList;
    ProgressDialog pd;

    public DosUtsAdapt(Context context) {
        this.context = context;
        dosenList = new ArrayList<>();
    }

    public DosUtsAdapt(List<Dosen> dosenList) {
        this.dosenList = dosenList;
    }

    public List<Dosen> getDosenList() {
        return dosenList;
    }

    public void setDosenList(List<Dosen> dosenList) {
        this.dosenList = dosenList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_card_uts,parent,false);

        return new  ViewHolder(v,parent.getContext());
    }

    @Override
    public void onBindViewHolder(@NonNull DosUtsAdapt.ViewHolder holder, int position) {
        Dosen d = dosenList.get(position);
        holder.tvNamaM.setText(d.getNama());
        holder.tvEmailM.setText(d.getEmail());
        holder.dos=d;
        pd = new ProgressDialog(holder.context1);
        holder.cToolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dosen d = dosenList.get(position);
                PopupMenu popupMenu = new PopupMenu(holder.context1,holder.cToolbar);
                popupMenu.inflate(R.menu.menu_card);
                popupMenu.show();
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                        switch (menuItem.getItemId()){
                            case R.id.CMenuDel:
                                pd.setTitle("Deleting");
                                pd.show();
                                Call<DefaultResult> del= service.delete_dos(
                                        d.getNidn().toString(),
                                        "72180186"

                                );
                                del.enqueue(new Callback<DefaultResult>() {
                                    @Override
                                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                                        pd.dismiss();
                                        Toast.makeText(holder.context1,"Success",Toast.LENGTH_LONG).show();
                                        Intent intent = new Intent(holder.context1, MainUtsActivity.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        holder.context1.startActivity(intent);
                                    }

                                    @Override
                                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                                        pd.dismiss();
                                        Toast.makeText(holder.context1,"Failed",Toast.LENGTH_LONG).show();
                                    }
                                });
                                break;
                            case R.id.CMenuView:
                                Intent UpInput =new Intent(holder.context1, DosViewActivity.class);
                                UpInput.putExtra("nim",d.getNidn());
                                UpInput.putExtra("nama",d.getNama());
                                UpInput.putExtra("alamat",d.getAlamat());
                                UpInput.putExtra("Email",d.getEmail());
                                UpInput.putExtra("Gelar",d.getGelar());
                                holder.context1.startActivity(UpInput);
                            break;
                            case  R.id.CMenuEdit:
                                Intent EdInput =new Intent(holder.context1, EditDosActivity.class);
                                EdInput.putExtra("nim",d.getNidn());
                                EdInput.putExtra("nama",d.getNama());
                                EdInput.putExtra("alamat",d.getAlamat());
                                EdInput.putExtra("Email",d.getEmail());
                                EdInput.putExtra("Gelar",d.getGelar());
                                holder.context1.startActivity(EdInput);
                        }
                        return false;
                    }
                });
            }
        });
    }

    @Override
    public int getItemCount() {
        return dosenList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView tvNamaM,tvEmailM;
        private ImageView cToolbar;
        Context context1;
        Dosen dos;
        public ViewHolder(@NonNull View itemView,Context context) {
            super(itemView);
            tvNamaM = itemView.findViewById(R.id.tvNamaM);
            tvEmailM = itemView.findViewById(R.id.tvEmailM);
            cToolbar = itemView.findViewById(R.id.OptionCard);
            context1=context;


        }
    }
}
