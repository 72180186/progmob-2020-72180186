package com.example.progmob2020.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Mtk {
    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("kode")
    @Expose
    private String kode;

    @SerializedName("nama")
    @Expose
    private String nama;

    @SerializedName("hari")
    @Expose
    private int hari;

    @SerializedName("sesi")
    @Expose
    private int sesi;

    @SerializedName("sks")
    @Expose
    private int sks;


    @SerializedName("nim_progmob")
    @Expose
    private String nim_progmob;

    public Mtk(String nama, String kode) {
        this.nama = nama;
        this.kode = kode;
    }

    public Mtk(String id, String nama, String kode, int hari, int sesi, int sks, String nim_progmob) {
        this.id = id;
        this.nama = nama;
        this.kode = kode;
        this.hari = hari;
        this.sesi = sesi;
        this.sks = sks;
        this.nim_progmob = nim_progmob;
    }

    public Mtk(String nama, String kode, int hari, int sesi, int sks, String nim_progmob) {
        this.nama = nama;
        this.kode = kode;
        this.hari = hari;
        this.sesi = sesi;
        this.sks = sks;
        this.nim_progmob = nim_progmob;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getkode() {
        return kode;
    }

    public void setkode(String kode) {
        this.kode = kode;
    }

    public String gethari() {
        return String.valueOf(hari);
    }

    public void sethari(int hari) {
        this.hari = hari;
    }

    public String getsesi() {
        return String.valueOf(sesi);
    }

    public void setsesi(int sesi) {
        this.sesi = sesi;
    }

    public String getsks() {
        return String.valueOf(sks);
    }

    public void setsks(int sks) {
        this.sks = sks;
    }

    public String getNim_progmob() {
        return nim_progmob;
    }

    public void setNim_progmob(String nim_progmob) {
        this.nim_progmob = nim_progmob;
    }
}
