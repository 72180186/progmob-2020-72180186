package com.example.progmob2020;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class HelpActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);

        TextView textHelp = (TextView)findViewById(R.id.textViewHlp);
        Bundle b = getIntent().getExtras();
        String texthlp = b.getString("hlp_string");
        textHelp.setText(texthlp);

    }
}