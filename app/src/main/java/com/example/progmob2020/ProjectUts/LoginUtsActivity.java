package com.example.progmob2020.ProjectUts;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.progmob2020.Crud.MahasiswaAddActivity;
import com.example.progmob2020.MainActivity;
import com.example.progmob2020.Model.DefaultResult;
import com.example.progmob2020.Model.LoginCred;
import com.example.progmob2020.Model.Mahasiswa;
import com.example.progmob2020.Network.GetDataService;
import com.example.progmob2020.Network.RetrofitClientInstance;
import com.example.progmob2020.Pertemuan2.RecyclerActivity;
import com.example.progmob2020.Pertemuan6.PrefActivity;
import com.example.progmob2020.R;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginUtsActivity extends AppCompatActivity {
    ProgressDialog pd;
    String isLogin="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_uts);

        EditText Nimlog =(EditText)findViewById(R.id.logNmTx);
        EditText psslog =(EditText)findViewById(R.id.logPassTx);
        Button btnLogin =(Button)findViewById((R.id.btnLogU));
        pd=new ProgressDialog(LoginUtsActivity.this);

        SharedPreferences pref = LoginUtsActivity.this.getSharedPreferences("prefs_file",MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();

        isLogin =pref.getString("isLogin","0");
        if(isLogin.equals("1")) {
            Intent intent = new Intent(LoginUtsActivity.this, MainUtsActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pd.setTitle("Logging In");
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<List<LoginCred>> login= service.Login(
                       Nimlog.getText().toString(),
                       psslog.getText().toString()
                );
                login.enqueue(new Callback<List<LoginCred>>() {
                    @Override
                    public void onResponse(Call<List<LoginCred>> call, Response<List<LoginCred>> response) {

                        if(response.body().size()!=0) {
                            editor.putString("isLogin", "1");
                            editor.commit();
                            pd.dismiss();
                            Intent intent = new Intent(LoginUtsActivity.this, MainUtsActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            Toast.makeText(LoginUtsActivity.this, "success", Toast.LENGTH_LONG).show();
                        }else {
                            pd.dismiss();
                            Toast.makeText(LoginUtsActivity.this,"failed",Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<List<LoginCred>> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(LoginUtsActivity.this,"failed",Toast.LENGTH_LONG).show();
                    }
                });

            }
        });

    }
}