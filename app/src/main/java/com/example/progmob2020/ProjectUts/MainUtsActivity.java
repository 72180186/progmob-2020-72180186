package com.example.progmob2020.ProjectUts;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import com.example.progmob2020.Pertemuan6.PrefActivity;
import com.example.progmob2020.R;
import com.google.android.material.snackbar.Snackbar;

public class MainUtsActivity extends AppCompatActivity {
ConstraintLayout currentLayout;
String isLogin="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_uts);
        ImageButton btnmhs = (ImageButton)findViewById(R.id.btnMhsUts) ;
        ImageButton btndos = (ImageButton)findViewById(R.id.btnDosUts) ;
        ImageButton btnMtk = (ImageButton)findViewById(R.id.btnMtkUts) ;
        Toolbar mytoolbar = (Toolbar)findViewById(R.id.toolbarMain);
        setSupportActionBar(mytoolbar);

        btnmhs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainUtsActivity.this, MhsUtsActivity.class);
                startActivity(intent);
            }
        });
        btndos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainUtsActivity.this, DosUtsActivity.class);
                startActivity(intent);
            }
        });
        btnMtk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainUtsActivity.this, MtkUtsActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        currentLayout = (ConstraintLayout)findViewById(R.id.constraintLayoutMain);
        SharedPreferences pref = MainUtsActivity.this.getSharedPreferences("prefs_file",MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        switch (item.getItemId()){
            case R.id.LogoutIB:
                //Snackbar.make(currentLayout,"tekan exit",Snackbar.LENGTH_LONG).show();
                AlertDialog.Builder builder =new AlertDialog.Builder(MainUtsActivity.this);
                builder.setIcon(R.drawable.ic_baseline_warning_24);
                builder.setTitle("Logout");
                builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                });
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        isLogin =pref.getString("isLogin","0");
                        editor.putString("isLogin", "0");
                        editor.commit();
                        Intent intent = new Intent(MainUtsActivity.this, LoginUtsActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    }
                });
                AlertDialog alertDialog = builder.create();
                alertDialog.show();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
