package com.example.progmob2020.ProjectUts;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.progmob2020.Model.DefaultResult;
import com.example.progmob2020.Network.GetDataService;
import com.example.progmob2020.Network.RetrofitClientInstance;
import com.example.progmob2020.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddMtkActivity extends AppCompatActivity {
    ProgressDialog pd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_mtk);
        TextView Tkd = (TextView)findViewById(R.id.txtadKd);
        TextView Tnam = (TextView)findViewById(R.id.txtadNamM);
        TextView Th = (TextView)findViewById(R.id.txtadHa);
        TextView Ts = (TextView)findViewById(R.id.txtadS);
        TextView Tsk = (TextView)findViewById(R.id.txtadSk);
        Button BtnUp = (Button)findViewById(R.id.btnUtsAddM);
        pd = new ProgressDialog(AddMtkActivity.this);
        BtnUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pd.setTitle("Please wait");
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> add= service.add_mtk(
                        Tnam.getText().toString(),
                        "72180186",
                        Tkd.getText().toString(),
                        Th.getText().toString(),
                        Ts.getText().toString(),
                        Tsk.getText().toString()

                );
                add.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(AddMtkActivity.this,"Success",Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(AddMtkActivity.this, MainUtsActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(AddMtkActivity.this,"Failed",Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }
}