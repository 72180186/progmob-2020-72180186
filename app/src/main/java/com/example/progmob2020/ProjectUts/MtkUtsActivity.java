package com.example.progmob2020.ProjectUts;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.progmob2020.Adapter.MahasiswaUtsAdapt;
import com.example.progmob2020.Adapter.MtkUtsAdapt;
import com.example.progmob2020.Model.Mahasiswa;
import com.example.progmob2020.Model.Mtk;
import com.example.progmob2020.Network.GetDataService;
import com.example.progmob2020.Network.RetrofitClientInstance;
import com.example.progmob2020.R;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MtkUtsActivity extends AppCompatActivity {

    RecyclerView rvMtk;
    MtkUtsAdapt mtkAdapter;
    ProgressDialog pd;
    List<Mtk> mtkList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mtk_uts);
        rvMtk = (RecyclerView)findViewById(R.id.rvMtk);
        pd = new ProgressDialog(this);
        pd.setTitle("Please Wait");
        pd.show();
        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<List<Mtk>> call = service.getmtk("72180186");
        Toolbar mytoolbar = (Toolbar)findViewById(R.id.toolbarMtk);
        setSupportActionBar(mytoolbar);
        call.enqueue(new Callback<List<Mtk>>() {
            @Override
            public void onResponse(Call<List<Mtk>> call, Response<List<Mtk>> response) {
                pd.dismiss();
                mtkList = response.body();
                mtkAdapter = new MtkUtsAdapt(mtkList);


                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(MtkUtsActivity.this);
                rvMtk.setLayoutManager(layoutManager);
                rvMtk.setAdapter(mtkAdapter);
            }

            @Override
            public void onFailure(Call<List<Mtk>> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(MtkUtsActivity.this,"Error",Toast.LENGTH_LONG);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_add,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.addMhsIB:
                Intent intent = new Intent(MtkUtsActivity.this, AddMtkActivity.class);
                startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }
}