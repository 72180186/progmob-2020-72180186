package com.example.progmob2020.ProjectUts;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.progmob2020.Adapter.DosUtsAdapt;
import com.example.progmob2020.Model.Dosen;
import com.example.progmob2020.Network.GetDataService;
import com.example.progmob2020.Network.RetrofitClientInstance;
import com.example.progmob2020.R;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DosUtsActivity extends AppCompatActivity {
    RecyclerView rvDos;
    DosUtsAdapt DosAdapter;
    ProgressDialog pd;
    List<Dosen> dosenList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dos_uts);
        rvDos = (RecyclerView)findViewById(R.id.rvDosUts);
        pd = new ProgressDialog(this);
        pd.setTitle("Please Wait");
        pd.show();
        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<List<Dosen>> call = service.getDosen("72180186");
        Toolbar mytoolbar = (Toolbar)findViewById(R.id.toolbarDos);
        setSupportActionBar(mytoolbar);
        call.enqueue(new Callback<List<Dosen>>() {
            @Override
            public void onResponse(Call<List<Dosen>> call, Response<List<Dosen>> response) {
                pd.dismiss();
                dosenList = response.body();
                DosAdapter = new DosUtsAdapt(dosenList);


                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(DosUtsActivity.this);
                rvDos.setLayoutManager(layoutManager);
                rvDos.setAdapter(DosAdapter);
            }

            @Override
            public void onFailure(Call<List<Dosen>> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(DosUtsActivity.this,"Error",Toast.LENGTH_LONG);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_add,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.addMhsIB:
                Intent intent = new Intent(DosUtsActivity.this, AddDosActivity.class);
                startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }
}