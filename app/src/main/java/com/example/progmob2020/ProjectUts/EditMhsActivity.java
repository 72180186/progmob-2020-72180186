package com.example.progmob2020.ProjectUts;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.progmob2020.Crud.MahasiswaUpdateActivity;
import com.example.progmob2020.Crud.MainMhsActivity;
import com.example.progmob2020.Model.DefaultResult;
import com.example.progmob2020.Network.GetDataService;
import com.example.progmob2020.Network.RetrofitClientInstance;
import com.example.progmob2020.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditMhsActivity extends AppCompatActivity {
    ProgressDialog pd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_mhs);
        TextView TnimL = (TextView)findViewById(R.id.txtEditNimL);
        TextView Tnim = (TextView)findViewById(R.id.txteNim);
        TextView Tnam = (TextView)findViewById(R.id.txteNam);
        TextView Tal = (TextView)findViewById(R.id.txteAl);
        TextView Tem = (TextView)findViewById(R.id.txteEmal);
        Button BtnUp = (Button)findViewById(R.id.btnUtsUpdate);
        pd = new ProgressDialog(EditMhsActivity.this);
        Intent data = getIntent();
        if(data != null) {
            TnimL.setText(data.getStringExtra("nim"));
            Tnim.setText(data.getStringExtra("nim"));
            Tnam.setText(data.getStringExtra("nama"));
            Tal.setText(data.getStringExtra("alamat"));
            Tem.setText(data.getStringExtra("Email"));
        }
        BtnUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pd.setTitle("Please Wait");
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> del= service.delete_mhs(
                        TnimL.getText().toString(),
                        "72180186"

                );
                del.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        Toast.makeText(EditMhsActivity.this,"Berhasil disimpan",Toast.LENGTH_LONG);
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(EditMhsActivity.this,"gagal disimpan",Toast.LENGTH_LONG);
                    }

                });
                Call<DefaultResult> add= service.add_mhs(
                        Tnam.getText().toString(),
                        Tnim.getText().toString(),
                        Tal.getText().toString(),
                        Tem.getText().toString(),
                        "kosongkan saja",
                        "72180186"
                );
                add.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(EditMhsActivity.this,"Success",Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(EditMhsActivity.this, MainUtsActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(EditMhsActivity.this,"Failed",Toast.LENGTH_LONG);
                    }
                });
            }
        });
    }
}