package com.example.progmob2020.ProjectUts;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.progmob2020.Model.DefaultResult;
import com.example.progmob2020.Network.GetDataService;
import com.example.progmob2020.Network.RetrofitClientInstance;
import com.example.progmob2020.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditDosActivity extends AppCompatActivity {
    ProgressDialog pd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_dos);
        TextView TnimL = (TextView)findViewById(R.id.txtEditNidn);
        TextView Tnim = (TextView)findViewById(R.id.txteNidn);
        TextView Tnam = (TextView)findViewById(R.id.txteNamD);
        TextView Tal = (TextView)findViewById(R.id.txteAlD);
        TextView Tem = (TextView)findViewById(R.id.txteEmalD);
        TextView Tge = (TextView)findViewById(R.id.txteGel);
        Button BtnUp = (Button)findViewById(R.id.btnUtsUpdateD);
        pd = new ProgressDialog(EditDosActivity.this);
        Intent data = getIntent();
        if(data != null) {
            TnimL.setText(data.getStringExtra("nim"));
            Tnim.setText(data.getStringExtra("nim"));
            Tnam.setText(data.getStringExtra("nama"));
            Tal.setText(data.getStringExtra("alamat"));
            Tem.setText(data.getStringExtra("Email"));
            Tge.setText(data.getStringExtra("Gelar"));
        }
        BtnUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pd.setTitle("Please wait");
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> del= service.delete_dos(
                        TnimL.getText().toString(),
                        "72180186"

                );
                del.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {

                        Toast.makeText(EditDosActivity.this,"Berhasil disimpan",Toast.LENGTH_LONG);

                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) { Toast.makeText(EditDosActivity.this,"Berhasil disimpan",Toast.LENGTH_LONG);
                        pd.dismiss();
                        Toast.makeText(EditDosActivity.this,"gagal disimpan",Toast.LENGTH_LONG).show();
                    }
                });
                Call<DefaultResult> add= service.add_dos(
                        Tnam.getText().toString(),
                        Tnim.getText().toString(),
                        Tal.getText().toString(),
                        Tem.getText().toString(),
                        Tge.getText().toString(),
                        "kosongkan saja",
                        "72180186"
                );
                add.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Intent intent = new Intent(EditDosActivity.this, MainUtsActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        Toast.makeText(EditDosActivity.this,"Success",Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(EditDosActivity.this,"Failed",Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }
}