package com.example.progmob2020.ProjectUts;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import com.example.progmob2020.R;

public class MtkViewActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mtk_view);
        TextView Tkode = (TextView)findViewById(R.id.txtvKode);
        TextView Tnam = (TextView)findViewById(R.id.txtvNamaM);
        TextView Th = (TextView)findViewById(R.id.txtvH);
        TextView Tse = (TextView)findViewById(R.id.txtvS);
        TextView Ts = (TextView)findViewById(R.id.txtvSk);
        Intent data = getIntent();
        if(data != null) {

            Tkode.setText(data.getStringExtra("kode"));
            Tnam.setText(data.getStringExtra("nama"));
            Th.setText(data.getStringExtra("hari"));
            Tse.setText(data.getStringExtra("sesi"));
            Ts.setText(data.getStringExtra("sks"));
        }
    }
}