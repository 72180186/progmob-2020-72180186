package com.example.progmob2020.ProjectUts;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.progmob2020.Model.DefaultResult;
import com.example.progmob2020.Network.GetDataService;
import com.example.progmob2020.Network.RetrofitClientInstance;
import com.example.progmob2020.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditMtkActivity extends AppCompatActivity {
    ProgressDialog pd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_mtk);
        TextView TnkdL = (TextView)findViewById(R.id.txtEditkodL);
        TextView Tkd = (TextView)findViewById(R.id.txteKd);
        TextView Tnam = (TextView)findViewById(R.id.txteNamM);
        TextView Th = (TextView)findViewById(R.id.txteHa);
        TextView Ts = (TextView)findViewById(R.id.txteS);
        TextView Tsk = (TextView)findViewById(R.id.txteSk);
        Button BtnUp = (Button)findViewById(R.id.btnUtsUpdateM);
        pd = new ProgressDialog(EditMtkActivity.this);
        Intent data = getIntent();
        if(data != null) {
            TnkdL.setText(data.getStringExtra("kode"));
            Tkd.setText(data.getStringExtra("kode"));
            Tnam.setText(data.getStringExtra("nama"));
            Th.setText(data.getStringExtra("hari"));
            Ts.setText(data.getStringExtra("sesi"));
            Tsk.setText(data.getStringExtra("sks"));
        }
        BtnUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pd.setTitle("Please Wait");
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> del= service.delete_mtk(
                        TnkdL.getText().toString(),
                        "72180186"

                );
                del.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        Toast.makeText(EditMtkActivity.this,"Berhasil disimpan",Toast.LENGTH_LONG);
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        Toast.makeText(EditMtkActivity.this,"gagal disimpan",Toast.LENGTH_LONG);
                    }
                });
               /* int hr,se,sk;
                hr=Integer.parseInt(Th.getText().toString());
                hr=Integer.parseInt(Th.getText().toString());
                hr=Integer.parseInt(Th.getText().toString());*/
                Call<DefaultResult> add= service.add_mtk(
                        Tnam.getText().toString(),
                        "72180186",
                        Tkd.getText().toString(),
                        Th.getText().toString(),
                        Ts.getText().toString(),
                        Tsk.getText().toString()

                );
                add.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(EditMtkActivity.this,"Success",Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(EditMtkActivity.this, MainUtsActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(EditMtkActivity.this,"Failed",Toast.LENGTH_LONG);
                    }
                });
            }
        });

    }
}