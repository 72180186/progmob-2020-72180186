package com.example.progmob2020.ProjectUts;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import com.example.progmob2020.R;

public class MhsViewActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mhs_view);
        TextView Tnim = (TextView)findViewById(R.id.txtvNim);
        TextView Tnam = (TextView)findViewById(R.id.txtvNama);
        TextView Tal = (TextView)findViewById(R.id.txtvAl);
        TextView Tem = (TextView)findViewById(R.id.txtxvEmail);

        Intent data = getIntent();
        if(data != null) {

            Tnim.setText(data.getStringExtra("nim"));
            Tnam.setText(data.getStringExtra("nama"));
            Tal.setText(data.getStringExtra("alamat"));
            Tem.setText(data.getStringExtra("Email"));
        }
    }
}