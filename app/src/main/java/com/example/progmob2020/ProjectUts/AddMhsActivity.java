package com.example.progmob2020.ProjectUts;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.progmob2020.Model.DefaultResult;
import com.example.progmob2020.Network.GetDataService;
import com.example.progmob2020.Network.RetrofitClientInstance;
import com.example.progmob2020.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddMhsActivity extends AppCompatActivity {
    ProgressDialog pd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_mhs);

        TextView nim = (TextView)findViewById(R.id.txtadNim);
        TextView nam = (TextView)findViewById(R.id.txtadNam);
        TextView alm = (TextView)findViewById(R.id.txtadAl);
        TextView eml = (TextView)findViewById(R.id.txtadEmal);
        Button sub =(Button)findViewById(R.id.btnUtsAdd);
        pd = new ProgressDialog(AddMhsActivity.this);
        sub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pd.setTitle("Please Wait");
                pd.show();
                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> add= service.add_mhs(
                        nam.getText().toString(),
                        nim.getText().toString(),
                        alm.getText().toString(),
                        eml.getText().toString(),
                        "kosongkan saja",
                        "72180186"
                );
                add.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(AddMhsActivity.this,"Success",Toast.LENGTH_LONG);
                        Intent intent = new Intent(AddMhsActivity.this, MainUtsActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(AddMhsActivity.this,"Failed",Toast.LENGTH_LONG);
                    }
                });
            }
        });
    }
}