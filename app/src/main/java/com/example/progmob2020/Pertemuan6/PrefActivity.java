package com.example.progmob2020.Pertemuan6;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.progmob2020.R;

public class PrefActivity extends AppCompatActivity {

    String isLogin="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pref);
        Button btnLog = (Button)findViewById(R.id.btnPref6);

        SharedPreferences pref =PrefActivity.this.getSharedPreferences("prefs_file",MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();


        isLogin =pref.getString("isLogin","0");
        if(isLogin.equals("1")){
            btnLog.setText("Logout");
        }else{
            btnLog.setText("Login");
        }
        btnLog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isLogin =pref.getString("isLogin","0");
                if(isLogin.equals("0")) {
                    editor.putString("isLogin", "1");
                    btnLog.setText("Logout");
                    //editor.commit();
                }else{
                    editor.putString("isLogin", "0");
                    btnLog.setText("Login");
                    //editor.commit();
                }
                editor.commit();
            }
        });
    }
}