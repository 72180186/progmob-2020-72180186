package com.example.progmob2020.Pertemuan2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.example.progmob2020.Adapter.MahasiswaRecyclerAdapter;
import com.example.progmob2020.Model.Mahasiswa;
import com.example.progmob2020.R;

import java.util.ArrayList;
import java.util.List;

public class RecyclerActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler);

        RecyclerView rv = (RecyclerView)findViewById(R.id.rvLat);
        MahasiswaRecyclerAdapter mahasiswaRecyclerAdapter;
         //dummy
        List<Mahasiswa> mahasiswaList =new ArrayList<Mahasiswa>();
        //gener
        Mahasiswa m1 = new Mahasiswa("Adrian","72180186","579238548932");
        Mahasiswa m2 = new Mahasiswa("Stefano","72180800","579238668932");
        Mahasiswa m3 = new Mahasiswa("Mahameru","72180886","579238458932");
        Mahasiswa m4 = new Mahasiswa("Davin","72180986","579238549832");
        Mahasiswa m5 = new Mahasiswa("Setiawan","72180786","579238548921");

        mahasiswaList.add(m1);
        mahasiswaList.add(m2);
        mahasiswaList.add(m3);
        mahasiswaList.add(m4);
        mahasiswaList.add(m5);

        mahasiswaRecyclerAdapter = new MahasiswaRecyclerAdapter(RecyclerActivity.this);
        mahasiswaRecyclerAdapter.setMahasiswaList(mahasiswaList);

        rv.setLayoutManager(new LinearLayoutManager(RecyclerActivity.this));
        rv.setAdapter(mahasiswaRecyclerAdapter);
    }
}